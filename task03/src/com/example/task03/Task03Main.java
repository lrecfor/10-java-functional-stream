package com.example.task03;

import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.stream.Stream;
import java.util.function.Consumer;
import java.util.concurrent.atomic.AtomicInteger;

public class Task03Main {

    public static void main(String[] args) {

        findMinMax(
                Stream.of(2, 9, 5, 4, 8, 1, 3),
                Integer::compareTo,
                (min, max) ->
                        System.out.println("min: " + min + " / max: " + max)
        );

    }

    public static <T> void findMinMax(
            Stream<? extends T> stream,
            Comparator<? super T> order,
            BiConsumer<? super T, ? super T> minMaxConsumer) {

        AtomicInteger len = new AtomicInteger(0);
        T[] min = (T[]) new Object[1];
        T[] max = (T[]) new Object[1];

        Consumer<T> consumer = obj -> {
            if (len.get() == 0 || order.compare(min[0], obj) > 0) {
                min[0] = obj;
            }
            if (len.get() == 0 || order.compare(max[0], obj) < 0) {
                max[0] = obj;
            }
            len.incrementAndGet();
        };

        stream.forEach(consumer);
        if (len.get() != 0) {
            minMaxConsumer.accept(min[0], max[0]);
        } else {
            minMaxConsumer.accept(null, null);
        }
    }
}
