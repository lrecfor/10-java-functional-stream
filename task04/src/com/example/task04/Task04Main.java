package com.example.task04;


import java.util.*;
import java.util.Map.Entry;
public class Task04Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in, "UTF-8");
        scanner.useDelimiter("[^\\p{L}]+");

        Map<String, Integer> wordCountMap = new HashMap<>();

        while (scanner.hasNext()) {
            String word = scanner.next().toLowerCase();
            wordCountMap.merge(word, 1, Integer::sum);
        }

        wordCountMap.entrySet().stream()
                .sorted(Comparator.comparing(Entry<String, Integer>::getValue)
                        .reversed()
                        .thenComparing(Entry::getKey))
                .map(Entry::getKey)
                .limit(10)
                .forEach(System.out::println);
    }
}